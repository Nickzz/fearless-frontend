import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [name, setName] = useState("");
    const [start, setStart] = useState("");
    const [end, setEnd] = useState("");
    const [description, setDescription] = useState("");
    const [maxPresentations, setMaxPresentations] = useState(0);
    const [maxAttendees, setMaxAttendees] = useState(0);
    const [location, setLocation] = useState([]);
    const [selectedLocation, setSelectedLocation] = useState("");

    //state variable to track if form is submitted succesfully
    const [isSubmissionSuccess, setIsSubmissionSuccess] = useState(false);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handlePresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setSelectedLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create a json data to POST to server
        const data = {};
        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = selectedLocation;

        const url = 'http://localhost:8000/api/locations/';

        const responseL = await fetch(url);
        if (responseL.ok) {
            const collected_data = await responseL.json();
            for (let location of collected_data.locations) {
                if (data.location == location.name) {
                    const url = location.href;
                    const parts = url.split('/'); // Split the string into an array by '/'
                    const id = parseInt(parts[3], 10);
                    data.location = id;
                }
            }

        }
        // define url and configure the data which would be sent to the server
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        // GET data from server and assign them to state in component
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newconference = await response.json();
            setName('');
            setStart('');
            setEnd('');
            setDescription('');
            setMaxPresentations(0);
            setMaxAttendees(0);
            setLocation([]);
            setSelectedLocation('');
            setIsSubmissionSuccess(true);
        }
        else { setIsSubmissionSuccess(false); }
    }


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocation(data.locations);

        }

    }

    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={start} onChange={handleStartChange} placeholder="start_date" required type="date" id="start_date" name="starts" className="form-control" />
                                <label htmlFor="start_date">Start Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={end} onChange={handleEndChange} placeholder="end_date" required type="date" id="end_date" name="ends" className="form-control" />
                                <label htmlFor="end_date">End Date</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="description">Description</label>
                                <textarea value={description} onChange={handleDescriptionChange} className="form-control" id="description" name="description" rows="3" required></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={maxPresentations} onChange={handlePresentationsChange} placeholder="max Presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control" />
                                <label htmlFor="max_presentation">max Presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={maxAttendees} onChange={handleAttendeesChange} placeholder="max Attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control" />
                                <label htmlFor="max_attendees">max Attendees</label>
                            </div>
                            <div className="mb-3">
                                <select value={selectedLocation} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {
                                        location.map(location => {
                                            return (
                                                <option key={location.href} value={location.name}>
                                                    {location.name}
                                                </option>
                                            );
                                        })
                                    }
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                            {isSubmissionSuccess && <div className="alert alert-success mt-3">Conference created successfully!</div>}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );

}
export default ConferenceForm;
