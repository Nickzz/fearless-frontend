import logo from './logo.svg';
import './App.css';

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <a className="navbar-brand" href="#">Conference GO!</a>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className="nav-link" href="/">Home</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/new-conference">New conference</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/new-presentation">New presentation</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


    );
}
export default Nav;
