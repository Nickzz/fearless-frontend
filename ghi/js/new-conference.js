
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById('location');
        for (let location of data.locations) {
            let option = document.createElement('option');

            const url = location.href;
            const parts = url.split('/'); // Split the string into an array by '/'
            const id = parseInt(parts[3], 10);
            option.value = id;

            option.innerHTML = location.name;
            selectTag.appendChild(option)
        }
        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            console.log(formData);
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(json);
            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'applications/json',
                },
            };
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
                console.log(newConference);
            }
        });
    }
});
